# OpenBSD-PC Engines APU4

This is some crap I wrote/discovered in the process of installing OpenBSD 6.8 stable onto my new PC Engines APU4. Documentation referenced:
- https://www.openbsd.org/faq/faq5.html - Building the System from Source
- https://www.openbsd.org/stable.html - Compiling `-stable`
- https://man.openbsd.org/make - `man 1 make`
- https://www.openbsd.org/anoncvs.html - Anonymous CVS



So far I've got:
- some dotfiles
- mk.conf

Maybe later I'll have:
- performance testing
- kernel modifications

Right now I'm:
- compiling /usr/src, making sure this mk.conf isn't broken (and just because it _compiles_ doesn't mean it _works_...)


On running `fw_update`:
- As of 2021-04-11, the only package identified is `vmm-firmware`. I wouldn't really recommend using this computer as a VM host, so it's up to you whether or not you want to install this firmware.

On compiling the kernel (without changes):
- This went pretty smoothly? Did that before I thought about doing documentation.
- Might be worth it to compare `-O_` feature levels (currently using -O3, I think), but I don't have any great ideas on how to compare kernel performance yet.

On compiling `/usr/src`:
- Sometimes if you use `-j5` or `-j4` to compile with multiple threads, you'll end up getting errors like, `cfparse.h` is unwriteable. So if you want to fix those, I would recommend the following (as `root`):
  - (this issue is documented and I think it's one of the reasons the OpenBSD developers discourage the use of the ports framework in lieu of `pkg_add`)
  * `rm -rf /usr/obj/*`
  * `cd /usr/src`
  * `make clean && make -j5 obj`
  * `time make -j5 build`
- Then, in a second shell, after the `make clean` step has finished (the part where you see a bunch of `rm` commands scroll by), while `make build` is running, do these:
  - (The chance of this happening is somewhat random, so this may not be necessary or there may be others not listed here)
  * `cd /usr/src/usr.sbin/mrouted && doas make -j5 && cd /usr/src`
    - This command works with `-j5` even though it doesn't in `make build`, and it should only take a few seconds
    - When the `make build` process gets to this point in the compilation process, it should(?) skip this package from being compiled and move on to the next one
- Time to completion (with `-j5`): 1065m39.63s real  3589m38.83s user   352m37.00s system
  - Just under 18 hours. This is after a few attempts (the first one I did used only one thread and also didn't include `-march`/`-mtune`/`-O`, and that took a few _days_), and may take longer before gcc/clang is compiled with optimizations
  - I'm not yet using this box as a router/firewall, but I would strongly recommend not using 4/5 compilation threads while expecting to also use the internet

On compiling ports:
- `sysutils/cpuid` doesn't compile. Might need to look into this.

`# pkg_info -APq` =>
```
devel/autoconf/2.67
devel/autoconf/2.69
devel/automake/1.16
devel/bison
archivers/bzip2
sysutils/colorls
sysutils/cpuid
sysutils/dmidecode
devel/dwz
lang/gcc/8,-libs
devel/gettext,-runtime
devel/gettext,-tools
devel/gmake
devel/gmp,-main
devel/help2man
net/iperf3
devel/libexecinfo
devel/libffi
converters/libiconv
devel/libtool,-ltdl
devel/libmpc
devel/libsigsegv
devel/gettext,-textstyle
devel/libtool,-main
devel/libyaml
archivers/lzip/lunzip
devel/m4
devel/metaauto
devel/mpfr
devel/pcre
databases/sqlports,-list
devel/py-setuptools,python3
lang/python/3.8,-main
devel/quirks
lang/ruby/2.6,-main
benchmarks/speedtest-cli
databases/sqlite3
editors/vim,-main,no_x11,perl,python3,ruby
sysutils/firmware/vmm
archivers/xz
shells/zsh
```

`# dmidecode -t processor` =>
```
SMBIOS 3.0 present.

Handle 0x0004, DMI type 4, 48 bytes
Processor Information
        Socket Designation: CPU0
        Type: Central Processor
        Family: Pentium Pro
        Manufacturer: AuthenticAMD
        ID: 01 0F 73 00 FF FB 8B 17
        Signature: Type 0, Family 22, Model 48, Stepping 1
        Flags:
                FPU (Floating-point unit on-chip)
                VME (Virtual mode extension)
                DE (Debugging extension)
                PSE (Page size extension)
                TSC (Time stamp counter)
                MSR (Model specific registers)
                PAE (Physical address extension)
                MCE (Machine check exception)
                CX8 (CMPXCHG8 instruction supported)
                APIC (On-chip APIC hardware supported)
                SEP (Fast system call)
                MTRR (Memory type range registers)
                PGE (Page global enable)
                MCA (Machine check architecture)
                CMOV (Conditional move instruction supported)
                PAT (Page attribute table)
                PSE-36 (36-bit page size extension)
                CLFSH (CLFLUSH instruction supported)
                MMX (MMX technology supported)
                FXSR (FXSAVE and FXSTOR instructions supported)
                SSE (Streaming SIMD extensions)
                SSE2 (Streaming SIMD extensions 2)
                HTT (Multi-threading)
        Version: AMD GX-412TC SOC
        Voltage: Unknown
        External Clock: Unknown
        Max Speed: Unknown
        Current Speed: Unknown
        Status: Populated, Enabled
        Upgrade: Unknown
        L1 Cache Handle: 0x0006
        L2 Cache Handle: 0x0007
        L3 Cache Handle: Not Provided
        Serial Number: Not Specified
        Asset Tag: Not Specified
        Part Number: Not Specified
        Characteristics:
                Hardware Thread
                Execute Protection
```

(using "120GB SSD M-Sata 120GB TLC Kingfast", part `msata120b`)
`#disklabel -h sd0` =>
```
# /dev/rsd0c:
type: SCSI
disk: SCSI disk
label: KingFast
duid: ################
flags:
bytes/sector: 512
sectors/track: 63
tracks/cylinder: 255
sectors/cylinder: 16065
cylinders: 14593
total sectors: 234441648 # total bytes: 111.8G
boundstart: 64
boundend: 234436545
drivedata: 0

16 partitions:
#                size           offset  fstype [fsize bsize   cpg]
  a:             1.0G               64  4.2BSD   2048 16384 12960 # /
  b:             8.0G          2104512    swap                    # none
  c:           111.8G                0  unused
  d:             8.0G         18892448  4.2BSD   2048 16384 12960 # /tmp
  e:            40.0G         35680352  4.2BSD   2048 16384 12960 # /var
  f:             8.0G        119571776  4.2BSD   2048 16384 12960 # /usr
  g:             1.0G        136359712  4.2BSD   2048 16384 12960 # /usr/X11R6
  h:            20.0G        138464224  4.2BSD   2048 16384 12960 # /usr/local
  i:             4.0G        180409920  4.2BSD   2048 16384 12960 # /usr/src
  j:             8.0G        188811936  4.2BSD   2048 16384 12960 # /usr/obj
  k:            13.8G        205599840  4.2BSD   2048 16384 12960 # /home
```

`% cat /etc/fstab` =>
```
91ec16b37772363c.b none swap sw
91ec16b37772363c.a / ffs rw 1 1
91ec16b37772363c.k /home ffs rw,nodev,nosuid 1 2
91ec16b37772363c.d /tmp ffs rw,nodev,nosuid 1 2
91ec16b37772363c.f /usr ffs rw,nodev 1 2
91ec16b37772363c.g /usr/X11R6 ffs rw,nodev 1 2
91ec16b37772363c.h /usr/local ffs rw,wxallowed,nodev 1 2
91ec16b37772363c.j /usr/obj ffs rw,nodev,nosuid 1 2
91ec16b37772363c.i /usr/src ffs rw,nodev,nosuid 1 2
91ec16b37772363c.e /var ffs rw,nodev,nosuid 1 2
```
